# movie-reviews (WBS SoSe 2020)

- Autor: Lars Wächter
- Anwendung: [https://movie-reviews-lw.herokuapp.com](https://movie-reviews-lw.herokuapp.com)

![preview](./screenshots/screenshot_02.png)

## Einführung

Die Mashup-App ist eine Webanwendung, welche zwei REST APIs miteinander verbindet:
Zum einem eine Schnittstelle der US Tageszeitung New York Times, welche Film Rezessionen zur Verfügung stellt. Zum anderen "The Open Movie Database" (OMDb), welche den Zugriff auf Informationen wie Schauspieler oder Bewertungen von Filmen ermöglicht.

Mittels der Mashup-App lässt sich nach Film Rezessionen der NYT suchen. Neben den Rezessionen können außerdem noch Details zu den jeweiligen Filmen angezeigt werden.

## REST APIs

### New York Times (NYT)

[https://developer.nytimes.com/docs/movie-reviews-api/1/overview](https://developer.nytimes.com/docs/movie-reviews-api/1/overview)

Die Schnittstelle der New York Times bietet einen Service, um Rezessionen zu Filmen abzufragen. Dieser liefert allerdings immer nur eine kleine Zusammenfassung und einen Link zur Rezession zurück, weshalb diese nicht innerhalb der Mashup-App betrachtet werden kann. Möchte man eine Rezession lesen, muss man erst den dazugehörigen Link öffnen.

### Open Movie Database (OMDb)

[https://www.omdbapi.com](https://www.omdbapi.com)

Die Schnittstelle der Open Movie Database bietet Zugriff auf Informationen zu einer Vielzahl an Filmen. Sie liefert unter anderem folgende Details eines Films:

- Dauer
- Veröffentlichung
- Zusammenfassung
- Schauspieler
- Bewertungen
- Auszeichnungen

## Funktionsweise

In der Suchleiste lässt sich nach Filmtiteln suchen. Nach Eingabe eines Titels bzw. Suchbegriffs wird ein `HTTP GET` Request an die API der NYT gesendet:

`https://api.nytimes.com/svc/movies/v2/reviews/search.json`

Dabei wird der eingegebene Suchbegriff als Parameter an die URL angehängt.

Die API gibt anschließend eine Liste mit übereinstimmenden Filmen zurück, welche als "Karten" dem Benutzer angezeigt werden. Eine Karte beinhaltet eine kleine Zusammenfassung des Films sowie einen Link zur Rezession der New York Times.

Des Weiteren kann sich der Benutzer Details zu dem Film anzeigen lassen. Zu diesem Zweck wird die API der Open Movie Database verwendet. Hierfür wird ein
`HTTP GET` Request an die Schnittstelle der OMDb gesendet:

`https://www.omdbapi.com`

Als URL Parameter wird der Titel und das Erscheinungsjahr, welche man zuvor von der Schnittstelle der NYT erhalten hat, des jeweiligen Films mitgegeben. Daraufhin öffnet sich ein Modal mit den Details zum Film. Sollten keine Details zu dem Film gefunden worden sein, wird dies dem Benutzer mit einem kleinen Hinweis-Text mitgeteilt.

## Verwendete Technologien

- React (Frontend JS Framework)
- Bootstrap (CSS Framework)
- Heroku (Hosting)
- Git (Versionsverwaltung & CI / CD)

## Suchbeispiele

Folgend ein paar Beispiel Filme, welche in die Suche eingegeben werden können:

- Star Wars
- Pulp Fiction
- Harry Potter
- The Lord of the Rings

Hinweis: Die Filmtitel müssen Englisch sein

## Farbpalette

zu finden in `src/scss/variables.scss`

```scss
$primary: #ff7043;
$primary-darker: #e5643c;

$secondary: #8d6e63;
$secondary-darker: #7e6359;

$surface: #1d1d1d;

$background: #121212;
$background-input: #3a3b3c;

$error: #cf6679;

$onPrimary: #000000;
$onSecondary: #ffffff;

$onSurface: rgba(255, 255, 255, 0.87);
$onSurfaceMediumEmphasis: rgba(255, 255, 255, 0.6);
$onSurfaceLowEmphasis: rgba(255, 255, 255, 0.38);

$onBackground: rgba(255, 255, 255, 0.6);

$onError: #000000;

$divider: #333333;

/* Google Material Design elevation: https://gist.github.com/serglo/f9f0be9a66fd6755a0bda85f9c64e85f */
$shadow-2dp: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
$shadow-4dp: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.2);
$shadow-6dp: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.2);
```

## Installation

### Standard

Um die App zu installieren wird Node.js und der Paketmanager npm benötigt.

1. Clone das GIT-Repository
2. Öffne ein Terminal im Ordner des Projekts
3. Gib `npm install` in das Terminal ein, um die Dependencies zu installieren
4. Benenne die Datei `.env.example` in `.env` um und trage die Umgebungsvariablen ein (siehe Moodle Abgabe)
5. Gib `npm start` in das Terminal ein, um die Anwendung zu starten
6. Die Anwendung sollte nun unter `http://localhost:3000` verfügbar sein

### Docker

Hierfür wird sowohl Docker als auch docker-compose benötigt:

1. Clone das GIT-Repository
2. Öffne ein Terminal im Ordner des Projekts
3. Benenne die Datei `.env.example` in `.env` um und trage die Umgebungsvariablen ein (siehe Moodle Abgabe)
4. Gib `docker-compose up` in das Terminal ein, um den Container und die
   Anwendung zu starten
5. Die Anwendung sollte nun unter `http://localhost:3000` verfügbar sein

## Sonstige Hinweise

- Es müssen englische Filmtitel in die Suche eingegeben werden
- 750ms nach der letzten Eingabe in der Suche, wird diese ausgeführt
- Die Anwendung lässt sich auch als PWA installieren
