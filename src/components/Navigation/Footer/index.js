import React, { useState } from 'react';

import './Footer.scss';

import Modal from '../../Modal';
import Imprint from '../../Information/Imprint';
import Privacy from '../../Information/Privacy';

const Footer = () => {
	const [isOpen, setIsOpen] = useState(false);
	const [modalContent, setModlaContent] = useState({
		title: null,
		content: null
	});

	const openModal = (title, content) => {
		setIsOpen(true);
		setModlaContent({
			title,
			content
		});
	};

	const closeModal = () => {
		setIsOpen(false);
		setModlaContent({
			title: null,
			content: null
		});
	};

	const openImprint = () => {
		openModal('Imprint', <Imprint />);
	};

	const openPrivacy = () => {
		openModal('Privacy', <Privacy />);
	};

	return (
		<div className="footer">
			<div className="container">
				<div className="row">
					<div className="col-12 col-md mb-5 mb-md-0">
						<h5 className="title">movie reviews and details</h5>
					</div>
					<div className="col-6 col-md-auto col-xl-2">
						<h4 className="h5">Contact</h4>
						<hr className="m-0 my-2" />
						<ul className="list-unstyled">
							<li>
								<a href="mailto:lars.waechter@mni.thm.de">Author</a>
							</li>
							<li>
								<a href="https://www.thm.de">THM</a>
							</li>
						</ul>
					</div>
					<div className="col-6 col-md-auto col-xl-2">
						<h4 className="h5">Information</h4>
						<hr className="m-0 my-2" />
						<ul className="list-unstyled">
							<li>
								<a href="#" onClick={openImprint}>
									Imprint
								</a>
							</li>
							<li>
								<a href="#" onClick={openPrivacy}>
									Privacy
								</a>
							</li>
						</ul>
					</div>
					<div className="col-12 col-md-auto col-xl-2 mt-4 mt-md-0">
						<h4 className="h5">Developers</h4>
						<hr className="m-0 my-2" />
						<ul className="list-unstyled">
							<li>
								<a href="https://developer.nytimes.com" target="_blank" rel="noopener noreferrer">
									NYT
								</a>
							</li>
							<li>
								<a href="https://www.omdbapi.com" target="_blank" rel="noopener noreferrer">
									OMDb
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<Modal isOpen={isOpen} handleClose={closeModal} title={modalContent.title} large={false}>
				{modalContent.content}
			</Modal>
		</div>
	);
};

export default Footer;
