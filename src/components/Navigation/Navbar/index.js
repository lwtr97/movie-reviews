import React from 'react';

import './Navbar.scss';

const Navbar = () => (
	<nav className="navbar navbar-dark">
		<div className="container">
			<a className="navbar-brand mb-0 h1 mr-auto" href="/">
				Movie Reviews
			</a>
			<span className="navbar-text">via NYT and OMDb</span>
		</div>
	</nav>
);

export default Navbar;
