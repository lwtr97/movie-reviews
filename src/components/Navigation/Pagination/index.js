import React from 'react';

import './Pagination.scss';

const Pagination = ({ updateParentState, offset, has_more }) => {
	const handleNext = () => {
		updateParentState(offset + 20);
	};

	const handlePrev = () => {
		updateParentState(offset - 20);
	};

	return (
		<nav>
			<ul className="pagination justify-content-end">
				<li className={`page-item ${offset === 0 ? 'disabled' : ''}`}>
					<a className="page-link" onClick={handlePrev}>
						<i className="fas fa-chevron-left"></i>
					</a>
				</li>
				<li className={`page-item ${!has_more ? 'disabled' : ''}`}>
					<a className="page-link" onClick={handleNext}>
						<i className="fas fa-chevron-right"></i>
					</a>
				</li>
			</ul>
		</nav>
	);
};

export default Pagination;
