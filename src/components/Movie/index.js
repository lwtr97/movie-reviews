import React, { useState } from 'react';

import './Movie.scss';

import noImage from '../../images/no-image_small.png';

import { OMDbService } from '../../services/OMDb';
import { UtilityService } from '../../services/Utility';

const Movie = ({ movie, openModal }) => {
	const { display_title, publication_date, summary_short, multimedia, link } = movie;
	const { url } = link;
	const imgLink = multimedia ? multimedia.src : noImage;

	const [isLoading, setIsLoading] = useState(false);
	const [text, setText] = useState('Details');

	const service = new OMDbService();

	const handleDetails = async () => {
		try {
			setIsLoading(true);
			const res = await service.searchMovie(display_title, publication_date.split('-')[0]);
			setIsLoading(false);

			if (res.data.Response === 'False') {
				setText('Not found!');
				setTimeout(() => {
					setText('Details');
				}, 2500);
			} else {
				setText('Details');
				openModal(res.data);
			}
		} catch (err) {
			setIsLoading(false);
			setText('Error!');
			setTimeout(() => {
				setText('Details');
			}, 2500);
		}
	};

	return (
		<div className="Movie card" style={{ flex: 1 }}>
			<img className="card-img-top" src={imgLink} width="210" height="180" alt={`${display_title} preview`}></img>
			<div className="card-body">
				<h5 className="card-title">{display_title}</h5>
				<h6 className="card-subtitle mb-2">Publication: {publication_date}</h6>
				<p className="card-text">{UtilityService.cutString(summary_short)}</p>
			</div>
			<div className="card-footer d-flex align-items-center">
				{isLoading ? (
					<button className="btn btn-primary btn-block" type="button" disabled>
						<span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
					</button>
				) : (
					<button type="button" className="btn btn-primary btn-block" onClick={handleDetails}>
						{text}
					</button>
				)}
				<a href={url} className="btn btn-secondary btn-block ml-1 mt-0" target="_blank" rel="noopener noreferrer">
					Review
				</a>
			</div>
		</div>
	);
};

export default Movie;
