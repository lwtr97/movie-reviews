import React from 'react';

const Imprint = () => (
	<div className="Imprint">
		<p>Information in accordance with Section 5 TMG</p>
		<p>
			Lars Wächter <br />
			Goethestraße 17 <br />
			35516 Münzenberg
		</p>
		<h5>Contact Information</h5>
		<p>
			E-Mail: <a href="mailto:lars.waechter@mni.thm.de">lars.waechter@mni.thm.de</a> <br />
			Internet address: <a href="https://www.thm.de">https://www.thm.de</a>
		</p>
		<h5>Disclaimer</h5>
		<p>
			Accountability for content <br />
			The contents of our pages have been created with the utmost care. However, we cannot guarantee the contents'
			accuracy, completeness or topicality. According to statutory provisions, we are furthermore responsible for our
			own content on these web pages. In this matter, please note that we are not obliged to monitor the transmitted or
			saved information of third parties, or investigate circumstances pointing to illegal activity. Our obligations to
			remove or block the use of information under generally applicable laws remain unaffected by this as per §§ 8 to 10
			of the Telemedia Act (TMG).
		</p>
		<p>
			Accountability for links <br />
			Responsibility for the content of external links (to web pages of third parties) lies solely with the operators of
			the linked pages. No violations were evident to us at the time of linking. Should any legal infringement become
			known to us, we will remove the respective link immediately.
		</p>
		<p>
			Copyright <br />
			Our web pages and their contents are subject to German copyright law. Unless expressly permitted by law, every
			form of utilizing, reproducing or processing works subject to copyright protection on our web pages requires the
			prior consent of the respective owner of the rights. Individual reproductions of a work are only allowed for
			private use. The materials from these pages are copyrighted and any unauthorized use may violate copyright laws.
		</p>
		<p>
			Quelle:{' '}
			<a href="http://www.translate-24h.de" target="_blank">
				Übersetzungsdienst translate-24h.de
			</a>
		</p>
	</div>
);

export default Imprint;
