import React from 'react';

import './Alert.scss';

const Alert = ({ type, title, msg }) => (
	<div className={`alert alert-${type}`}>
		{title.length ? <strong>{title}</strong> : null} {msg}
	</div>
);

Alert.defaultProps = {
	title: ''
};

export default Alert;
