import React, { Component } from 'react';

import './Overview.scss';

import { NYTService } from '../../services/NYT';

import List from '../List';
import Pagination from '../Navigation/Pagination';
import Loader from '../Loader';
import Alert from '../Alert';

class Overview extends Component {
	constructor(props) {
		super(props);

		this.service = new NYTService();

		this.state = {
			data: null,
			offset: 0,
			isLoading: false,
			error: ''
		};
	}

	componentDidMount = async () => {
		const { searchTerm } = this.props;
		if (searchTerm.length) await this.fetchMovies();
	};

	componentDidUpdate = async (prevProps) => {
		if (prevProps.searchTerm !== this.props.searchTerm || prevProps.searchOrder !== this.props.searchOrder)
			await this.fetchMovies(this.props.searchTerm, this.props.searchOrder);
	};

	handlePagination = (offset) => {
		const { searchTerm, searchOrder } = this.props;

		this.setState({ offset });
		this.fetchMovies(searchTerm, searchOrder, offset);
	};

	fetchMovies = async (term, order, offset = 0) => {
		if (!term.length) this.setState({ data: null });
		else {
			this.setState({ isLoading: true, data: null, error: '' }, async () => {
				try {
					const res = await this.service.searchMovie(term, order, offset);
					this.setState({ data: res.data, isLoading: false });
				} catch (err) {
					this.setState({
						isLoading: false,
						error: 'Failed to fetch data.'
					});
				}
			});
		}
	};

	render() {
		const { data, offset, isLoading, error } = this.state;

		if (isLoading) return <Loader />;

		if (error.length)
			return (
				<div className="row">
					<div className="col-12 offset-0 col-md-6 offset-md-3">
						<Alert title="Error!" msg={error} type="danger" />
					</div>
				</div>
			);

		if (data === null) return null;

		if (!data.results.length) return <div className="hint">No results</div>;

		return (
			<div className="Overview">
				<List movies={data.results} />
				<Pagination updateParentState={this.handlePagination} offset={offset} has_more={data.has_more} />
			</div>
		);
	}
}

export default Overview;
