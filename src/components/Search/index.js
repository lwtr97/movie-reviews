import React from 'react';

import './Search.scss';

const Search = (props) => {
	const { updateParentTerm, updateParentOrder } = props;
	let timeout;

	const handleBlur = async (e) => {
		const term = e.target.value.trim();
		updateParentTerm(term);
	};

	const handleChange = async (e) => {
		const term = e.target.value.trim();

		clearTimeout(timeout);

		timeout = setTimeout(() => {
			updateParentTerm(term);
		}, 750);
	};

	const handleOrderChange = (e) => {
		updateParentOrder(e.target.value);
	};

	return (
		<div className="Search">
			<div className="row my-5">
				<div className="col-12 col-md-6 offset-md-2">
					<div className="input-group">
						<input
							type="text"
							className="form-control"
							placeholder="Search movie..."
							aria-label="Search movie"
							aria-describedby="basic-addon1"
							onBlur={handleBlur}
							onChange={handleChange}
							id="search"
							autoComplete="off"
							autoFocus
						/>
						<div className="input-group-append">
							<span className="input-group-text" id="basic-addon1">
								<i className="fas fa-search"></i>
							</span>
						</div>
					</div>
				</div>
				<div className="col-12 col-md-2 mt-2 mt-md-0">
					<div className="input-group">
						<select className="form-control" onChange={handleOrderChange} aria-label="Search order">
							<option value="">order-by</option>
							<option value="by-opening-date">opening-date</option>
							<option value="by-publication-date">publication-date</option>
							<option value="by-title">title</option>
						</select>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Search;
