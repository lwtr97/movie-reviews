import { create } from 'axios';

export class OMDbService {
	constructor() {
		this.axios = create({
			baseURL: 'https://www.omdbapi.com'
		});
	}

	searchMovie = (title, year) => {
		return this.axios.get('/', {
			params: {
				apikey: process.env.REACT_APP_OMDB_KEY,
				t: title,
				y: year,
				plot: 'full'
			}
		});
	};
}
