export class UtilityService {
	static cutString = (string, length = 150) => {
		if (string.length <= 150) return string;

		let str = string.slice(0, 150);

		if (str[length - 1] !== '?' && str[length - 1] !== '.' && str[length - 1] !== '!') {
			for (let i = length; i < string.length; i++) {
				if (string[i] === ' ') break;
				str += string[i];
			}
		}

		return str.trim() + '...';
	};
}
