import { create } from 'axios';

export class NYTService {
	constructor() {
		this.axios = create({
			baseURL: 'https://api.nytimes.com/svc/movies/v2/'
		});
	}

	searchMovie = (title, order, offset = 0) => {
		return this.axios.get('reviews/search.json', {
			params: {
				query: title,
				order,
				offset,
				'api-key': process.env.REACT_APP_NYT_KEY
			}
		});
	};
}
